using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Text;

    public static class ExportPCI
    {
        [FunctionName("ExportPCI")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {

            string accessKey = "";
            string accountName = "";
            string containerName = "";
            string connectionString;
            CloudStorageAccount storageAccount;
            CloudBlobClient client;
            CloudBlobContainer container;
            CloudBlockBlob blob;

            HttpStatusCode returnCode;
            string returnBody;

            dynamic body = await req.Content.ReadAsStringAsync();
            var input_data = JsonConvert.DeserializeObject<PCIFile>(body as string);

            try
            {
                //uploading blob
                string Author = input_data.Author;
                string FileName = input_data.FileName;
                string DataBase64 = input_data.DataBase64;
                byte[] fileBytes = Convert.FromBase64String(DataBase64);

                //getting BLOB Container
                accountName = System.Environment.GetEnvironmentVariable("StorageAccountName");
                accessKey = System.Environment.GetEnvironmentVariable("AccessKey");
                containerName = System.Environment.GetEnvironmentVariable("storageContainer");
                connectionString = "DefaultEndpointsProtocol=https;AccountName=" + accountName + ";AccountKey=" + accessKey + ";EndpointSuffix=core.windows.net";
                storageAccount = CloudStorageAccount.Parse(connectionString);

                client = storageAccount.CreateCloudBlobClient();

                container = client.GetContainerReference(containerName);

                //Upload File content
                blob = container.GetBlockBlobReference(FileName);

                //create snapshot
                if (blob.Exists()) { 
                    blob.CreateSnapshot();
                }

                blob.Properties.ContentType = "text/plain";
                blob.Metadata["Author"] = Author;
                
                await blob.UploadFromByteArrayAsync(fileBytes,0, fileBytes.Length);

                //return success and URI
                returnCode = HttpStatusCode.OK;
                returnBody = blob.StorageUri.PrimaryUri.AbsoluteUri;


            }
            catch (Exception e)
            {
                // Exception 
                log.Error("Error occured during the opertion: " + e.ToString());
                returnCode = HttpStatusCode.BadRequest;
                returnBody = $"ERROR! " + e;
            }

            return req.CreateResponse(returnCode, returnBody);
        }
    }

    public class PCIFile
    {
        public string Author { get; set; }
        public string FileName { get; set; }
        public string DataBase64 { get; set; }
    }


